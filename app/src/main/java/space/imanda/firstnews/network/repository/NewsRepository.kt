package space.imanda.firstnews.network.repository

import space.imanda.firstnews.data.model.NewsResponse
import space.imanda.firstnews.network.api.ApiServices
import space.imanda.firstnews.network.state.NetworkState
import javax.inject.Inject

class NewsRepository @Inject constructor(
    private val remoteDataSource: ApiServices
) : INewsRepository {

    override suspend fun getNews(
        countryCode: String,
        pageNumber: Int,
        category: String
    ): NetworkState<NewsResponse> {
        return try {

            val response = remoteDataSource.getNews(countryCode, pageNumber, category)
            val result = response.body()
            if (response.isSuccessful && result !== null) {
                NetworkState.Success(result)
            } else {
                NetworkState.Error("An error occurred")
            }
        } catch (exception: Exception) {
            NetworkState.Error("Error occurred ${exception.localizedMessage}")
        }
    }

    override suspend fun searchNews(
        searchQuery: String,
        pageNumber: Int
    ): NetworkState<NewsResponse> {

        return try {
            val response = remoteDataSource.searchNews(searchQuery, pageNumber)
            val result = response.body()
            if (response.isSuccessful && result !== null) {
                NetworkState.Success(result)
            } else {
                NetworkState.Error("An error occurred")
            }
        } catch (exception: Exception) {
            NetworkState.Error("Error occurred ${exception.localizedMessage}")
        }

    }

}