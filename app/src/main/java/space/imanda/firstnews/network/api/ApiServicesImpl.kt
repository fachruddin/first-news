package space.imanda.firstnews.network.api

import retrofit2.Response
import space.imanda.firstnews.data.model.NewsResponse
import javax.inject.Inject

class ApiServicesImpl @Inject constructor(private val newsApi: NewsApi): ApiServices {

    override suspend fun getNews(countryCode: String, pageNumber: Int, category: String): Response<NewsResponse> =
        newsApi.getNews(countryCode, pageNumber, category)

    override suspend fun searchNews(query: String, pageNumber: Int): Response<NewsResponse> =
        newsApi.searchNews(query, pageNumber)

}