package space.imanda.firstnews.network.repository

import space.imanda.firstnews.data.model.NewsResponse
import space.imanda.firstnews.network.state.NetworkState

interface INewsRepository {

    suspend fun getNews(countryCode: String, pageNumber: Int, category: String): NetworkState<NewsResponse>

    suspend fun searchNews(searchQuery: String, pageNumber: Int): NetworkState<NewsResponse>

}