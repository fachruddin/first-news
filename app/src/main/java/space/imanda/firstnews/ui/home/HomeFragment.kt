package space.imanda.firstnews.ui.home

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.card.MaterialCardView
import space.imanda.firstnews.R
import space.imanda.firstnews.databinding.FragmentHomeBinding
import space.imanda.firstnews.network.state.NetworkState
import space.imanda.firstnews.ui.adapter.NewsAdapter
import space.imanda.firstnews.ui.main.MainActivity
import space.imanda.firstnews.ui.main.MainViewModel
import space.imanda.firstnews.utils.Constants
import space.imanda.firstnews.utils.Constants.Companion.QUERY_PER_PAGE
import space.imanda.firstnews.utils.EndlessRecyclerOnScrollListener
import space.imanda.firstnews.utils.NewsCategory

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private lateinit var onScrollListener: EndlessRecyclerOnScrollListener
    lateinit var mainViewModel: MainViewModel
    private lateinit var newsAdapter: NewsAdapter
    val countryCode = Constants.COUNTRY_CODE
    private lateinit var searchView: SearchView

    private var newsCategory: String = NewsCategory.BUSINESS.categoryName

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mainViewModel = (activity as MainActivity).mainViewModel
        setCategoryCard()
        isCategorySelected(true, binding.layoutCategory.cardBusiness, binding.layoutCategory.textViewBusiness)
        setupUI()
        setupRecyclerView()
        setupObservers()
        setHasOptionsMenu(true)

    }

    private fun setCategoryCard() {
        binding.layoutCategory.apply {
            setCategoryInfo(cardBusiness, textViewBusiness, NewsCategory.BUSINESS.categoryName)
            setCategoryInfo(cardEntertainment, textViewEntertainment, NewsCategory.ENTERTAINMENT.categoryName)
            setCategoryInfo(cardGeneral, textViewGeneral, NewsCategory.GENERAL.categoryName)
            setCategoryInfo(cardHealth, textViewHealth, NewsCategory.HEALTH.categoryName)
            setCategoryInfo(cardScience, textViewScience, NewsCategory.SCIENCE.categoryName)
            setCategoryInfo(cardSports, textViewSports, NewsCategory.SPORTS.categoryName)
            setCategoryInfo(cardTechnology, textViewTechnology, NewsCategory.TECHNOLOGY.categoryName)
        }
    }

    private fun setCategoryInfo(card: MaterialCardView, textView: AppCompatTextView, categoryName: String) {

        card.setOnClickListener {

            setUnSelectCategoryCard()
            newsCategory = categoryName

            mainViewModel.getNews(countryCode, newsCategory, true)
            isCategorySelected(true, card, textView)

        }
    }

    private fun setUnSelectCategoryCard() {
        with(binding.layoutCategory) {
            isCategorySelected(false, cardBusiness, textViewBusiness)
            isCategorySelected(false, cardEntertainment, textViewEntertainment)
            isCategorySelected(false, cardGeneral, textViewGeneral)
            isCategorySelected(false, cardHealth, textViewHealth)
            isCategorySelected(false, cardScience, textViewScience)
            isCategorySelected(false, cardSports, textViewSports)
            isCategorySelected(false, cardTechnology, textViewTechnology)
        }
    }

    private fun isCategorySelected(selected: Boolean, card: MaterialCardView, textView: AppCompatTextView) {
        if (selected) {
            card.setCardBackgroundColor(requireContext().getColor(R.color.black))
            textView.setTextColor(requireContext().getColor(R.color.white))
        } else {
            card.setCardBackgroundColor(requireContext().getColor(R.color.white))
            textView.setTextColor(requireContext().getColor(R.color.black))
        }
    }

    private fun setupUI() {
        binding.itemErrorMessage.btnRetry.setOnClickListener {
            if (mainViewModel.searchEnable) {
                mainViewModel.searchNews(mainViewModel.newQuery)
            } else {
                 mainViewModel.getNews(countryCode, newsCategory, false)
            }
            hideErrorMessage()
        }

        onScrollListener = object : EndlessRecyclerOnScrollListener(QUERY_PER_PAGE) {
            override fun onLoadMore() {
                if (mainViewModel.searchEnable) {
                    mainViewModel.searchNews(mainViewModel.newQuery)
                } else {
                    mainViewModel.getNews(countryCode, newsCategory, false)
                }
            }
        }

        val refreshListener = SwipeRefreshLayout.OnRefreshListener {

            try {
                binding.swipeRefreshLayout.isRefreshing = false
                mainViewModel.clearSearch()
                mainViewModel.getNews(countryCode, newsCategory, false)

                searchView.onActionViewCollapsed()
                searchView.maxWidth = 0

            } catch (exception: Exception) {
                exception.printStackTrace()
            }

        }
        binding.swipeRefreshLayout.setOnRefreshListener(refreshListener)
    }

    private fun setupRecyclerView() {
        newsAdapter = NewsAdapter()
        binding.rvNews.apply {
            adapter = newsAdapter
            layoutManager = LinearLayoutManager(activity)
            addOnScrollListener(onScrollListener)
        }
        newsAdapter.setOnItemClickListener { news ->
            val bundle = Bundle().apply {
                putSerializable("news", news)
            }
            findNavController().navigate(
                R.id.action_homeFragment_to_detailFragment,
                bundle
            )
        }
    }

    private fun setupObservers() {
        lifecycleScope.launchWhenStarted {
            if (!mainViewModel.searchEnable) {
                mainViewModel.newsResponse.collect { response ->
                    when (response) {
                        is NetworkState.Success -> {
                            hideProgressBar()
                            hideErrorMessage()
                            response.data?.let { newResponse ->

                                newsAdapter.differ.submitList(newResponse.articles.toList())
                                mainViewModel.totalPage =
                                    newResponse.totalResults / QUERY_PER_PAGE + 1
                                onScrollListener.isLastPage =
                                    mainViewModel.getNewsPage == mainViewModel.totalPage + 1
                                hideBottomPadding()
                            }
                        }

                        is NetworkState.Loading -> {
                            showProgressBar()
                        }

                        is NetworkState.Error -> {
                            hideProgressBar()
                            response.message?.let {
                                showErrorMessage(response.message)
                            }
                        }

                        else -> {}
                    }
                }
            } else {
                collectSearchResponse()
            }
        }

        lifecycleScope.launchWhenStarted {
            mainViewModel.errorMessage.collect { value ->
                if (value.isNotEmpty()) {
                    Toast.makeText(activity, value, Toast.LENGTH_LONG).show()
                }
                mainViewModel.hideErrorToast()
            }
        }
    }

    private fun collectSearchResponse() {
        //Search response
        lifecycleScope.launchWhenStarted {
            if (mainViewModel.searchEnable) {
                mainViewModel.searchNewsResponse.collect { response ->
                    when (response) {
                        is NetworkState.Success -> {
                            hideProgressBar()
                            hideErrorMessage()
                            response.data?.let { searchResponse ->

                                newsAdapter.differ.submitList(searchResponse.articles.toList())
                                mainViewModel.totalPage =
                                    searchResponse.totalResults / QUERY_PER_PAGE + 1
                                onScrollListener.isLastPage =
                                    mainViewModel.searchNewsPage == mainViewModel.totalPage + 1
                                hideBottomPadding()
                            }
                        }

                        is NetworkState.Loading -> {
                            showProgressBar()
                        }

                        is NetworkState.Error -> {
                            hideProgressBar()
                            response.message?.let {
                                showErrorMessage(response.message)
                            }
                        }

                        else -> {}
                    }
                }
            }
        }
    }

    private fun showProgressBar() {
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        binding.progressBar.visibility = View.GONE
    }

    private fun showErrorMessage(message: String) {
        binding.itemErrorMessage.errorCard.visibility = View.VISIBLE
        binding.itemErrorMessage.tvErrorMessage.text = message
        onScrollListener.isError = true
    }

    private fun hideErrorMessage() {
        binding.itemErrorMessage.errorCard.visibility = View.GONE
        onScrollListener.isError = false
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.toolbar_menu, menu)
        val searchItem = menu.findItem(R.id.action_search)
        searchView = searchItem.actionView as SearchView
        //Search button clicked
        searchView.setOnSearchClickListener {
            searchView.maxWidth = android.R.attr.width
        }
        //Close button clicked
        searchView.setOnCloseListener {
            mainViewModel.clearSearch()
            mainViewModel.getNews(countryCode, newsCategory, false)
            //Collapse the action view
            searchView.onActionViewCollapsed()
            searchView.maxWidth = 0
            true
        }

        val searchPlate =
            searchView.findViewById(androidx.appcompat.R.id.search_src_text) as EditText
        searchPlate.hint = "Search"
        val searchPlateView: View =
            searchView.findViewById(androidx.appcompat.R.id.search_plate)

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let {
                    mainViewModel.searchNews(query)
                    mainViewModel.enableSearch()
                    setUnSelectCategoryCard()
                    collectSearchResponse()
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })

        activity?.let {
            searchPlateView.setBackgroundColor(
                ContextCompat.getColor(
                    it,
                    android.R.color.transparent
                )
            )
            val searchManager =
                it.getSystemService(Context.SEARCH_SERVICE) as SearchManager
            searchView.setSearchableInfo(searchManager.getSearchableInfo(it.componentName))
        }
        //check if search is activated
        if (mainViewModel.searchEnable) {
            searchView.isIconified = false
            searchItem.expandActionView()
            searchView.setQuery(mainViewModel.newQuery, false)
        }
        return super.onCreateOptionsMenu(menu, inflater)
    }

    private fun hideBottomPadding() {
        if (onScrollListener.isLastPage) {
            binding.rvNews.setPadding(0, 0, 0, 0)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}