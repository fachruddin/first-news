package space.imanda.firstnews

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class FirstNewsApp: Application()